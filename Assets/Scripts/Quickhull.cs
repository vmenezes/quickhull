﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quickhull : MonoBehaviour {
    
    public List<Transform> originalPoints = new List<Transform>();

    public float pointRadius = 2f;

    public List<Transform> convexHull = new List<Transform>();

    private void Start()
    {
        GetChilds();
    }

    private void GetChilds()
    {
        originalPoints.Clear();
        
        foreach (Transform t in transform)
        {
            originalPoints.Add(t);
        }
    }

    private void Update()
    {
        QuickHull();
    }

    private void QuickHull()
    {
        List<Transform> points = new List<Transform>(originalPoints.ToArray());
        
        convexHull.Clear();

        if (points.Count < 3)
            convexHull.AddRange(points);

        // find extremals
        int minPoint = int.MaxValue, maxPoint = int.MinValue;
        float minX = float.MaxValue;
        float maxX = float.MinValue;

        for (int i = 0; i < points.Count; i++)
        {
            if (points[i].position.x < minX)
            {
                minX = points[i].position.x;
                minPoint = i;
            }
            if (points[i].position.x > maxX)
            {
                maxX = points[i].position.x;
                maxPoint = i;
            }
        }

        Transform A = points[minPoint];
        Transform B = points[maxPoint];
        convexHull.Add(A);
        convexHull.Add(B);

        points.Remove(A);
        points.Remove(B);

        List<Transform> leftSet = new List<Transform>();
        List<Transform> rightSet = new List<Transform>();

        for (int i = 0; i < points.Count; i++)
        {
            Transform p = points[i];
            if (pointLocation(A, B, p) == -1)
                leftSet.Add(p);
            else
                rightSet.Add(p);
        }
        hullSet(A, B, rightSet, convexHull);
        hullSet(B, A, leftSet, convexHull);
    }

    /*
     * Computes the square of the distance of point C to the segment defined by points AB
     */
    public float Distance(Transform A, Transform B, Transform C)
    {
        float ABx = B.position.x - A.position.x;
        float ABy = B.position.y - A.position.y;
        float num = ABx * (A.position.y - C.position.y) - ABy * (A.position.x - C.position.x);
        if (num < 0)
            num = -num;
        return num;
    }

    public void hullSet(Transform A, Transform B, List<Transform> set, List<Transform> hull)
    {
        int insertPosition = hull.IndexOf(B);
        if (set.Count == 0)
            return;
        if (set.Count == 1)
        {
            Transform p = set[0];
            set.Remove(p);
            hull.Insert(insertPosition, p);
            return;
        }

        float dist = float.MinValue;
        int furthestPoint = -1;

        for (int i = 0; i < set.Count; i++)
        {
            Transform p = set[i];
            float distance = Distance(A, B, p);
            if (distance > dist)
            {
                dist = distance;
                furthestPoint = i;
            }
        }
        Transform P = set[furthestPoint];

        set.RemoveAt(furthestPoint);

        hull.Insert(insertPosition, P);

        // Determine who's to the left of AP
        List<Transform> leftSetAP = new List<Transform>();

        for (int i = 0; i < set.Count; i++)
        {
            Transform M = set[i];
            if (pointLocation(A, P, M) == 1)
            {
                leftSetAP.Add(M);
            }
        }

        // Determine who's to the left of PB
        List<Transform> leftSetPB = new List<Transform>();

        for (int i = 0; i < set.Count; i++)
        {
            Transform M = set[i];
            if (pointLocation(P, B, M) == 1)
            {
                leftSetPB.Add(M);
            }
        }
        hullSet(A, P, leftSetAP, hull);
        hullSet(P, B, leftSetPB, hull);

    }

    public int pointLocation(Transform A, Transform B, Transform P)
    {
        float cp1 = (B.position.x - A.position.x) * (P.position.y - A.position.y) - (B.position.y - A.position.y) * (P.position.x - A.position.x);
        return (cp1 > 0) ? 1 : -1;
    }

    private void OnDrawGizmos()
    {
        if (originalPoints.Count == 0)
            return;

        Gizmos.color = Color.magenta;

        foreach (Transform t in originalPoints)
        {
            Gizmos.DrawWireSphere(t.position, pointRadius);
        }

        if (convexHull.Count == 0)
            return;

        Gizmos.color = Color.cyan;

        for (int i = 1; i < convexHull.Count; i++)
        {
            Gizmos.DrawLine(convexHull[i - 1].position, convexHull[i].position);
        }

        Gizmos.DrawLine(convexHull[convexHull.Count - 1].position, convexHull[0].position);
    }
}
